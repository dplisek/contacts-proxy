package org.plech.contactsProxy.services.impl;

import net.sourceforge.cardme.vcard.features.AddressFeature;
import net.sourceforge.cardme.vcard.types.AddressType;
import org.plech.contactsProxy.pojos.Address;
import org.plech.contactsProxy.services.AddressService;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements AddressService {

    @Override
    public Address createAddressFromFeature(AddressFeature addressFeature) {
        Address address = new Address();
        address.setStreetAddress(addressFeature.getStreetAddress());
        address.setPostalCode(addressFeature.getPostalCode());
        address.setCity(addressFeature.getLocality());
        address.setCountry(addressFeature.getCountryName());
        return address;
    }

    @Override
    public AddressFeature createFeatureFromAddress(Address address) {
        AddressType addressType = new AddressType();
        addressType.setStreetAddress(address.getStreetAddress());
        addressType.setPostalCode(address.getPostalCode());
        addressType.setLocality(address.getCity());
        addressType.setCountryName(address.getCountry());
        return addressType;
    }
}
