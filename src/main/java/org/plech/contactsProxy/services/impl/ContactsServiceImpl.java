package org.plech.contactsProxy.services.impl;

import net.sourceforge.cardme.engine.VCardEngine;
import net.sourceforge.cardme.io.VCardWriter;
import net.sourceforge.cardme.vcard.EncodingType;
import net.sourceforge.cardme.vcard.VCard;
import net.sourceforge.cardme.vcard.VCardImpl;
import net.sourceforge.cardme.vcard.features.*;
import net.sourceforge.cardme.vcard.types.*;
import net.sourceforge.cardme.vcard.types.media.ImageMediaType;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.plech.contactsProxy.dao.AddressBookDAO;
import org.plech.contactsProxy.dao.CardDAO;
import org.plech.contactsProxy.domain.AddressBook;
import org.plech.contactsProxy.domain.Card;
import org.plech.contactsProxy.pojos.Address;
import org.plech.contactsProxy.pojos.Contact;
import org.plech.contactsProxy.services.AddressService;
import org.plech.contactsProxy.services.ContactsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

@Service
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class ContactsServiceImpl implements ContactsService {

    private static final String HEXA_CHARS = "0123456789abcdef";
    public static final String X_GETS_POST_INVITES = "X-GETS-POST-INVITES";
    public static final String X_INTEREST_IN_ARTISTS = "X-INTEREST-IN-ARTISTS";

    private final Logger log = Logger.getLogger(this.getClass());

    private VCardEngine vCardEngine = new VCardEngine();
    private int latestCTag = -1;

    @Autowired
    private CardDAO cardDAO;

    @Autowired
    private AddressBookDAO addressBookDAO;

    @Autowired
    private AddressService addressService;

    @Value("${addressBookId}")
    private Integer addressBookId;

    @Override
    public VCard getVCardFromCard(Card card) throws IOException {
        byte[] cardData = card.getCardData();
        String vCardString = new String(cardData, CHARSET_NAME);
        return vCardEngine.parse(vCardString);
    }

    @Override
    public VCard getVCardFromContact(Contact contact) {
        VCardImpl vCard = new VCardImpl();
        vCard.setUID(new UIDType(generateUniqueUid()));
        updateVCardFromContact(vCard, contact);
        return vCard;
    }

    @Override
    public VCard updateVCardFromContact(VCard vCard, Contact contact) {
        vCard.setName(getNameTypeFromContact(contact));
        vCard.setFormattedName(getFormattedNameTypeFromContact(contact));
        vCard.setOrganizations(contact.getCompany() == null ? null : new OrganizationType(contact.getCompany()));
        vCard.clearNotes();
        if (contact.getNote() != null && !contact.getNote().isEmpty()) {
            vCard.addNote(new NoteType(contact.getNote()));
        }
        vCard.clearAddresses();
        vCard.addAllAddresses(getAddressesFromContact(contact));
        vCard.clearTelephoneNumbers();
        vCard.addAllTelephoneNumber(getPhoneNumbersFromContact(contact));
        vCard.clearEmails();
        vCard.addAllEmails(getEmailsFromContact(contact));
        vCard.clearPhotos();
        if (contact.getPhoto() != null) {
            vCard.addPhoto(getPhotoFromContact(contact));
        }
        updateReceivesPostInvites(vCard, contact);
        updateInterestInArtists(vCard, contact);
        return vCard;
    }

    private void updateReceivesPostInvites(VCard vCard, Contact contact) {
        for (Iterator<ExtendedFeature> extendedFeatureIterator = vCard.getExtendedTypes(); extendedFeatureIterator.hasNext(); ) {
            ExtendedFeature extendedFeature = extendedFeatureIterator.next();
            if (X_GETS_POST_INVITES.equals(extendedFeature.getExtensionName())) {
                extendedFeatureIterator.remove();
                break;
            }
        }
        vCard.addExtendedType(new ExtendedType(X_GETS_POST_INVITES, "" + contact.isReceivesPostInvites()));
    }

    private void updateInterestInArtists(VCard vCard, Contact contact) {
        for (Iterator<ExtendedFeature> extendedFeatureIterator = vCard.getExtendedTypes(); extendedFeatureIterator.hasNext(); ) {
            ExtendedFeature extendedFeature = extendedFeatureIterator.next();
            if (X_INTEREST_IN_ARTISTS.equals(extendedFeature.getExtensionName())) {
                extendedFeatureIterator.remove();
                break;
            }
        }
        if (contact.getInterestInArtistIds() != null && !contact.getInterestInArtistIds().isEmpty()) {
            vCard.addExtendedType(new ExtendedType(X_INTEREST_IN_ARTISTS, joinIds(contact.getInterestInArtistIds())));
        }
    }

    private String joinIds(Map<Long, Boolean> interestInArtistIds) {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<Long, Boolean> entry : interestInArtistIds.entrySet()) {
            if (entry.getValue() == null || !entry.getValue()) continue;
            if (builder.length() > 0) {
                builder.append(",");
            }
            builder.append(entry.getKey());
        }
        return builder.toString();
    }

    private PhotoFeature getPhotoFromContact(Contact contact) {
        PhotoFeature photoFeature = new PhotoType();
        photoFeature.setEncodingType(EncodingType.BINARY);
        photoFeature.setImageMediaType(ImageMediaType.JPEG);
        photoFeature.setPhoto(contact.getPhoto());
        return photoFeature;
    }

    private String generateUniqueUid() {
        String uid = generateUid();
        while (!isUidUnique(uid)) {
            uid = generateUid();
        }
        return uid;
    }

    private String generateUid() {
        StringBuilder builder = new StringBuilder();
        builder.append(RandomStringUtils.random(8, HEXA_CHARS));
        builder.append("-");
        builder.append(RandomStringUtils.random(4, HEXA_CHARS));
        builder.append("-");
        builder.append(RandomStringUtils.random(4, HEXA_CHARS));
        builder.append("-");
        builder.append(RandomStringUtils.random(4, HEXA_CHARS));
        builder.append("-");
        builder.append(RandomStringUtils.random(12, HEXA_CHARS));
        return builder.toString();
    }

    private boolean isUidUnique(String uid) {
        return cardDAO.countByUriLike(uid, addressBookId) == 0;
    }

    private NameFeature getNameTypeFromContact(Contact contact) {
        NameType nameType = new NameType();
        nameType.setGivenName(contact.getFirstName());
        nameType.setFamilyName(contact.getLastName());
        return nameType;
    }

    private FormattedNameFeature getFormattedNameTypeFromContact(Contact contact) {
        StringBuilder builder = new StringBuilder();
        builder.append(contact.getFirstName() == null ? "" : contact.getFirstName());
        if (contact.getFirstName() != null && contact.getLastName() != null) {
            builder.append(" ");
        }
        builder.append(contact.getLastName() == null ? "" : contact.getLastName());
        FormattedNameType formattedNameType = new FormattedNameType();
        formattedNameType.setFormattedName(builder.toString());
        return formattedNameType;
    }

    private Collection<AddressFeature> getAddressesFromContact(Contact contact) {
        Set<AddressFeature> addressFeatures = new HashSet<AddressFeature>();
        for (Address address : contact.getAddresses()) {
            addressFeatures.add(addressService.createFeatureFromAddress(address));
        }
        return addressFeatures;
    }

    private Collection<TelephoneFeature> getPhoneNumbersFromContact(Contact contact) {
        Set<TelephoneFeature> telephoneFeatures = new HashSet<TelephoneFeature>();
        for (String phoneNumber : contact.getPhoneNumbers()) {
            telephoneFeatures.add(new TelephoneType(phoneNumber));
        }
        return telephoneFeatures;
    }

    private Collection<EmailFeature> getEmailsFromContact(Contact contact) {
        Set<EmailFeature> emailFeatures = new HashSet<EmailFeature>();
        for (String email : contact.getEmails()) {
            emailFeatures.add(new EmailType(email));
        }
        return emailFeatures;
    }

    @Override
    public Contact getContactFromVCardWithId(VCard vCard, Integer id) {
        Contact contact = new Contact();
        contact.setId(id);
        contact.setFirstName(vCard.getName() == null ? null : vCard.getName().getGivenName());
        contact.setLastName(vCard.getName() == null ? null : vCard.getName().getFamilyName());
        contact.setFormattedName(vCard.getFormattedName().getFormattedName());
        contact.setCompany(getCompanyFromVCard(vCard));
        contact.setAddresses(getAddressesFromVCard(vCard));
        contact.setEmails(getEmailsFromVCard(vCard));
        contact.setPhoneNumbers(getPhoneNumbersFromVCard(vCard));
        contact.setPhoto(getPhotoFromVCard(vCard));
        contact.setReceivesPostInvites(isReceivesPostInvites(vCard));
        contact.setInterestInArtistIds(getInterestInArtists(vCard));
        contact.setNote(getNoteFromVCard(vCard));
        return contact;
    }

    private boolean isReceivesPostInvites(VCard vCard) {
        for (Iterator<ExtendedFeature> extendedFeatureIterator = vCard.getExtendedTypes(); extendedFeatureIterator.hasNext(); ) {
            ExtendedFeature extendedFeature = extendedFeatureIterator.next();
            if (X_GETS_POST_INVITES.equals(extendedFeature.getExtensionName())) {
                return Boolean.parseBoolean(extendedFeature.getExtensionData());
            }
        }
        return false;
    }

    private Map<Long, Boolean> getInterestInArtists(VCard vCard) {
        Map<Long, Boolean> interest = new HashMap<>();
        for (Iterator<ExtendedFeature> extendedFeatureIterator = vCard.getExtendedTypes(); extendedFeatureIterator.hasNext(); ) {
            ExtendedFeature extendedFeature = extendedFeatureIterator.next();
            if (X_INTEREST_IN_ARTISTS.equals(extendedFeature.getExtensionName())) {
                String[] interestInArtistIds = extendedFeature.getExtensionData().split(",");
                for (String interestInArtistId : interestInArtistIds) {
                    if (interestInArtistId.isEmpty()) continue;
                    interest.put(Long.parseLong(interestInArtistId), true);
                }
                return interest;
            }
        }
        return interest;
    }

    private byte[] getPhotoFromVCard(VCard vCard) {
        if (!vCard.getPhotos().hasNext()) return null;
        PhotoFeature photoFeature = vCard.getPhotos().next();
        return photoFeature.getPhoto();
    }

    @Override
    public Card getCardFromContactAndVCard(Contact contact, VCard vCard) throws UnsupportedEncodingException {
        Card card = new Card();
        card.setLastModified((int) (new Date().getTime() / 1000));
        card.setAddressBookId(addressBookId);
        card.setUri(vCard.getUID().getUID() + ".vcf");
        card.setCardData(getCardData(vCard));
        return card;
    }

    private byte[] getCardData(VCard vCard) throws UnsupportedEncodingException {
        VCardWriter writer = new VCardWriter();
        writer.setVCard(vCard);
        return writer.buildVCardString().getBytes(CHARSET_NAME);
    }

    private String getCompanyFromVCard(VCard vCard) {
        if (vCard.getOrganizations() == null) return null;
        if (!vCard.getOrganizations().getOrganizations().hasNext()) return null;
        return vCard.getOrganizations().getOrganizations().next();
    }

    private String getNoteFromVCard(VCard vCard) {
        if (vCard.getNotes() == null) return null;
        if (!vCard.getNotes().hasNext()) return null;
        return vCard.getNotes().next().getNote();
    }

    private List<Address> getAddressesFromVCard(VCard vCard) {
        List<Address> addresses = new ArrayList<Address>();
        for (Iterator<AddressFeature> addressFeatureIterator = vCard.getAddresses(); addressFeatureIterator.hasNext(); ) {
            AddressFeature addressFeature = addressFeatureIterator.next();
            addresses.add(addressService.createAddressFromFeature(addressFeature));
        }
        return addresses;
    }

    private List<String> getEmailsFromVCard(VCard vCard) {
        List<String> emails = new ArrayList<String>();
        for (Iterator<EmailFeature> emailFeatureIterator = vCard.getEmails(); emailFeatureIterator.hasNext(); ) {
            EmailFeature emailFeature = emailFeatureIterator.next();
            emails.add(emailFeature.getEmail());
        }
        return emails;
    }

    private List<String> getPhoneNumbersFromVCard(VCard vCard) {
        List<String> phoneNumbers = new ArrayList<String>();
        for (Iterator<TelephoneFeature> phoneNumberIterator = vCard.getTelephoneNumbers(); phoneNumberIterator.hasNext(); ) {
            TelephoneFeature phoneNumberFeature = phoneNumberIterator.next();
            phoneNumbers.add(phoneNumberFeature.getTelephone());
        }
        return phoneNumbers;
    }

    @Override
    public List<Card> getCards() {
        return cardDAO.getAllCardsFromAddressBook(addressBookId);
    }

    @Override
    public Card getCard(int id) {
        return cardDAO.getCard(id);
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void saveCard(Card card) {
        cardDAO.save(card);
        log.debug("New card saved.");
        incAddressBookCTag();
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void updateCard(Card card, VCard vCard) throws UnsupportedEncodingException {
        card.setCardData(getCardData(vCard));
        cardDAO.update(card);
        log.debug("Existing card updated.");
        incAddressBookCTag();
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void deleteCard(Card card) {
        cardDAO.delete(card);
        log.debug("Card deleted.");
        incAddressBookCTag();
    }

    @Override
    public boolean hasAddressBookCTagIncreased() {
        AddressBook addressBook = addressBookDAO.get(addressBookId);
        return addressBook.getCtag() > latestCTag;
    }

    private void incAddressBookCTag() {
        AddressBook addressBook = addressBookDAO.get(addressBookId);
        addressBook.setCtag(addressBook.getCtag() + 1);
        addressBookDAO.update(addressBook);
        latestCTag = addressBook.getCtag();
        log.debug("Address book cTag increased to " + latestCTag);
    }

    @Override
    public void fetchAddressBookCTag() {
        AddressBook addressBook = addressBookDAO.get(addressBookId);
        latestCTag = addressBook.getCtag();
        log.debug("Address book cTag now at " + latestCTag);
    }
}
