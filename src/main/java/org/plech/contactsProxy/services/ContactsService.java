package org.plech.contactsProxy.services;

import net.sourceforge.cardme.vcard.VCard;
import org.plech.contactsProxy.domain.Card;
import org.plech.contactsProxy.pojos.Contact;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public interface ContactsService {

    String CHARSET_NAME = "UTF-8";

    VCard getVCardFromCard(Card card) throws IOException;

    VCard getVCardFromContact(Contact contact);

    VCard updateVCardFromContact(VCard vCard, Contact contact);

    Contact getContactFromVCardWithId(VCard vCard, Integer id);

    Card getCardFromContactAndVCard(Contact contact, VCard vCard) throws UnsupportedEncodingException;

    List<Card> getCards();

    Card getCard(int id);

    void saveCard(Card card);

    void updateCard(Card card, VCard vCard) throws UnsupportedEncodingException;

    void deleteCard(Card card);

    boolean hasAddressBookCTagIncreased();

    void fetchAddressBookCTag();
}
