package org.plech.contactsProxy.services;

import net.sourceforge.cardme.vcard.features.AddressFeature;
import org.plech.contactsProxy.pojos.Address;

public interface AddressService {

    Address createAddressFromFeature(AddressFeature addressFeature);

    AddressFeature createFeatureFromAddress(Address address);
}
