package org.plech.contactsProxy.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "cards")
public class Card {

    @Id @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @NotNull
    @Column(name = "addressbookid")
    private Integer addressBookId;

    @Column(name = "carddata", columnDefinition = "mediumblob")
    private byte[] cardData;

    @Column(name = "uri", length = 200)
    private String uri;

    @Column(name = "lastmodified")
    private Integer lastModified;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAddressBookId() {
        return addressBookId;
    }

    public void setAddressBookId(Integer addressBookId) {
        this.addressBookId = addressBookId;
    }

    public byte[] getCardData() {
        return cardData;
    }

    public void setCardData(byte[] cardData) {
        this.cardData = cardData;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Integer getLastModified() {
        return lastModified;
    }

    public void setLastModified(Integer lastModified) {
        this.lastModified = lastModified;
    }
}
