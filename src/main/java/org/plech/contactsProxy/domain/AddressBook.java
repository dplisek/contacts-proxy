package org.plech.contactsProxy.domain;

import javax.persistence.*;

@Entity
@Table(name = "addressbooks")
public class AddressBook {

    @Id @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @Column(name = "ctag")
    private Integer ctag;

    public Integer getCtag() {
        return ctag;
    }

    public void setCtag(Integer ctag) {
        this.ctag = ctag;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
