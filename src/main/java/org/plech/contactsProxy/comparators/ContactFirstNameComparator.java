package org.plech.contactsProxy.comparators;

import org.plech.contactsProxy.pojos.Contact;

import java.util.Comparator;

public class ContactFirstNameComparator implements Comparator<Contact> {

    @Override
    public int compare(Contact o1, Contact o2) {
        if (o1.getFirstName() == null && o2.getFirstName() == null) return 0;
        if (o1.getFirstName() == null) return 1;
        if (o2.getFirstName() == null) return -1;
        return o1.getFirstName().compareToIgnoreCase(o2.getFirstName());
    }
}
