package org.plech.contactsProxy.comparators;

import org.plech.contactsProxy.pojos.Contact;

import java.util.Comparator;

public class ContactCompanyComparator implements Comparator<Contact> {

    @Override
    public int compare(Contact o1, Contact o2) {
        if (o1.getCompany() == null && o2.getCompany() == null) return 0;
        if (o1.getCompany() == null) return 1;
        if (o2.getCompany() == null) return -1;
        return o1.getCompany().compareToIgnoreCase(o2.getCompany());
    }
}
