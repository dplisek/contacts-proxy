package org.plech.contactsProxy.comparators;

import org.plech.contactsProxy.pojos.Contact;

import java.util.Comparator;

public class ContactLastNameComparator implements Comparator<Contact> {

    @Override
    public int compare(Contact o1, Contact o2) {
        if (o1.getLastName() == null && o2.getLastName() == null) return 0;
        if (o1.getLastName() == null) return 1;
        if (o2.getLastName() == null) return -1;
        return o1.getLastName().compareToIgnoreCase(o2.getLastName());
    }
}
