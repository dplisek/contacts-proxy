package org.plech.contactsProxy.controllers;

import net.sourceforge.cardme.vcard.VCard;
import org.apache.log4j.Logger;
import org.plech.contactsProxy.comparators.ContactCompanyComparator;
import org.plech.contactsProxy.comparators.ContactFirstNameComparator;
import org.plech.contactsProxy.comparators.ContactLastNameComparator;
import org.plech.contactsProxy.domain.Card;
import org.plech.contactsProxy.pojos.Contact;
import org.plech.contactsProxy.services.ContactsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("contacts")
public class ContactsController {

    @Autowired
    private ContactsService contactsService;

    private final Logger log = Logger.getLogger(this.getClass());

    private List<Contact> cache = new ArrayList<>();

    @RequestMapping(value = "list", method = RequestMethod.GET)
    @ResponseBody
    public synchronized List<Contact> list() throws IOException {
        if (contactsService.hasAddressBookCTagIncreased()) {
            updateCache();
        } else {
            log.debug("Addressbook cTag hasn't been updated, using cache.");
        }
        return cache;
    }

    private void updateCache() throws IOException {
        List<Card> cards = contactsService.getCards();
        log.debug("Fetched all " + cards.size() + " cards from storage.");
        cache.clear();
        for (Card card : cards) {
            VCard vCard = contactsService.getVCardFromCard(card);
            cache.add(contactsService.getContactFromVCardWithId(vCard, card.getId()));
        }
        Collections.sort(cache, new ContactCompanyComparator());
        Collections.sort(cache, new ContactFirstNameComparator());
        Collections.sort(cache, new ContactLastNameComparator());
        contactsService.fetchAddressBookCTag();
        log.debug("Cache updated, new size: " + cache.size());
    }

    @RequestMapping(value = "get", method = RequestMethod.GET)
    @ResponseBody
    public synchronized Contact get(Integer id) throws IOException {
        Card card = contactsService.getCard(id);
        VCard vCard = contactsService.getVCardFromCard(card);
        return contactsService.getContactFromVCardWithId(vCard, card.getId());
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public synchronized int save(@RequestBody Contact contact) throws IOException {
        VCard vCard = contactsService.getVCardFromContact(contact);
        Card card = contactsService.getCardFromContactAndVCard(contact, vCard);
        contactsService.saveCard(card);
        updateCache();
        return card.getId();
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public synchronized void update(@RequestBody Contact contact) throws IOException {
        Card card = contactsService.getCard(contact.getId());
        VCard vCard = contactsService.getVCardFromCard(card);
        contactsService.updateVCardFromContact(vCard, contact);
        contactsService.updateCard(card, vCard);
        updateCache();
    }

    @RequestMapping(value = "delete", method = RequestMethod.DELETE)
    @ResponseBody
    public synchronized void delete(@RequestParam Integer id) throws IOException {
        Card card = contactsService.getCard(id);
        contactsService.deleteCard(card);
        updateCache();
    }
}
