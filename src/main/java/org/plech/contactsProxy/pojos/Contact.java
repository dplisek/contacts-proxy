package org.plech.contactsProxy.pojos;

import java.util.List;
import java.util.Map;

public class Contact {

    private int id;
    private String firstName;
    private String lastName;
    private String formattedName;
    private String company;
    private boolean receivesPostInvites;
    private Map<Long, Boolean> interestInArtistIds;
    private List<Address> addresses;
    private List<String> emails;
    private List<String> phoneNumbers;
    private byte[] photo;
    private String note;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getFormattedName() {
        return formattedName;
    }

    public void setFormattedName(String formattedName) {
        this.formattedName = formattedName;
    }

    public boolean isReceivesPostInvites() {
        return receivesPostInvites;
    }

    public void setReceivesPostInvites(boolean receivesPostInvites) {
        this.receivesPostInvites = receivesPostInvites;
    }

    public Map<Long, Boolean> getInterestInArtistIds() {
        return interestInArtistIds;
    }

    public void setInterestInArtistIds(Map<Long, Boolean> interestInArtistIds) {
        this.interestInArtistIds = interestInArtistIds;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
