package org.plech.contactsProxy.dao;

import org.plech.contactsProxy.domain.Card;

import java.util.List;

public interface CardDAO {

    List<Card> getAllCardsFromAddressBook(int addressBookId);

    Card getCard(int id);

    int countByUriLike(String uri, int addressBookId);

    void save(Card card);

    void delete(Card card);

    void update(Card card);
}
