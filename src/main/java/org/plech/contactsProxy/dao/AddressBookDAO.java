package org.plech.contactsProxy.dao;

import org.plech.contactsProxy.domain.AddressBook;

public interface AddressBookDAO {

    AddressBook get(int addressBookId);

    void update(AddressBook addressBook);
}
