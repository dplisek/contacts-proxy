package org.plech.contactsProxy.dao.hibernate;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.plech.contactsProxy.dao.CardDAO;
import org.plech.contactsProxy.domain.Card;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CardDAOImpl implements CardDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Card> getAllCardsFromAddressBook(int addressBookId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select c from Card c where c.addressBookId = :addressBookId order by c.id");
        query.setParameter("addressBookId", addressBookId);
        return query.list();
    }

    @Override
    public Card getCard(int id) {
        return (Card) sessionFactory.getCurrentSession().get(Card.class, id);
    }

    @Override
    public int countByUriLike(String uri, int addressBookId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select count(*) from Card c where c.addressBookId = :addressBookId and c.uri like :uri");
        query.setParameter("addressBookId", addressBookId);
        query.setParameter("uri", uri);
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public void save(Card card) {
        sessionFactory.getCurrentSession().save(card);
    }

    @Override
    public void delete(Card card) {
        sessionFactory.getCurrentSession().delete(card);
    }

    @Override
    public void update(Card card) {
        sessionFactory.getCurrentSession().update(card);
    }
}
