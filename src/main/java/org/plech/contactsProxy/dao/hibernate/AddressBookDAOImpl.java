package org.plech.contactsProxy.dao.hibernate;

import org.hibernate.SessionFactory;
import org.plech.contactsProxy.dao.AddressBookDAO;
import org.plech.contactsProxy.domain.AddressBook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AddressBookDAOImpl implements AddressBookDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public AddressBook get(int addressBookId) {
        return (AddressBook) sessionFactory.getCurrentSession().get(AddressBook.class, addressBookId);
    }

    @Override
    public void update(AddressBook addressBook) {
        sessionFactory.getCurrentSession().update(addressBook);
    }
}
