package org.plech.contactsProxy.advice;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class ExceptionHandlerAdvice {

    private static final Logger log = Logger.getLogger(ExceptionHandlerAdvice.class);

    @ExceptionHandler
    public void handleException(Exception e, HttpServletResponse response) throws IOException {
        response.setStatus(500);
        log.error("Internal server error", e);
        response.getOutputStream().print(e.getMessage());
        response.flushBuffer();
    }
}
